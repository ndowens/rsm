PREFIX ?= /usr

.PHONY: all
all:
	@echo 'nothing to do'

.PHONY: man
man: man/rsm.8
man/vsv.8: man/rsm.md
	md2man-roff $^ > $@

.PHONY: clean
clean:
	rm -f man/rsm.8

.PHONY: install
install:
	install -DTm755 rsm $(PREFIX)/bin/rsm
	install -DTm644 man/rsm.8 $(PREFIX)/share/man/man8/rsm.8

.PHONY: uninstall
uninstall:
	rm -f $(PREFIX)/bin/rsm
	rm -f $(PREFIX)/share/man/man8/rsm.8

.PHONY: check
check:
	shellcheck rsm
